package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Echoer extends Thread {
	private Socket socket;
	private VerificareUtilizator verif;
	
	public Echoer(Socket socket) {
		this.socket = socket;
		verif = new VerificareUtilizator();
		
	}
	public void run() {
		try {
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
			
			String user = input.readLine();
			String pass = input.readLine();
			
			if(verif.validate(user, pass))
				output.println("Utilizatorul este valid !");
			else
				output.println("Utilizatorul nu este valid !");
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}


class VerificareUtilizator {
	private String parola;
	private String numeUtilizator;
	
	public VerificareUtilizator() {
		numeUtilizator = "alex";
		parola = "fotbalistul77";
	}
	
	public boolean validate(String user, String pass) {
		return parola.equals(pass) && numeUtilizator.equals(user);
	}

}
