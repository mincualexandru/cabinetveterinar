package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-01-14T23:00:41.036+0200")
@StaticMetamodel(Animal.class)
public class Animal_ {
	public static volatile SingularAttribute<Animal, Integer> idAnimal;
	public static volatile SingularAttribute<Animal, String> age;
	public static volatile SingularAttribute<Animal, String> breed;
	public static volatile SingularAttribute<Animal, byte[]> image;
	public static volatile SingularAttribute<Animal, String> name;
	public static volatile ListAttribute<Animal, Programari> programaris;
}
