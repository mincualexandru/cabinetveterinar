package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-01-14T14:01:16.224+0200")
@StaticMetamodel(Programari.class)
public class Programari_ {
	public static volatile SingularAttribute<Programari, Integer> idprogramari;
	public static volatile SingularAttribute<Programari, Date> data;
	public static volatile SingularAttribute<Programari, Animal> animal;
	public static volatile SingularAttribute<Programari, Personalmedical> personalmedical;
	public static volatile SingularAttribute<Programari, String> diagnostic;
	public static volatile SingularAttribute<Programari, Integer> ora;
}
