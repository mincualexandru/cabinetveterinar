package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the personalmedical database table.
 * 
 */
@Entity
@NamedQuery(name="Personalmedical.findAll", query="SELECT p FROM Personalmedical p")
public class Personalmedical implements Serializable {
	private static final long serialVersionUID = 1L;
	private int idPersonalMedical;
	private String function;
	private String name;
	private int salary;
	private List<Programari> listaProgramari;

	@Override
	public String toString() {
		return name;
	}


	public Personalmedical() {
	}


	@Id
	public int getIdPersonalMedical() {
		return this.idPersonalMedical;
	}

	public void setIdPersonalMedical(int idPersonalMedical) {
		this.idPersonalMedical = idPersonalMedical;
	}


	public String getFunction() {
		return this.function;
	}

	public void setFunction(String function) {
		this.function = function;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public int getSalary() {
		return this.salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}


	//bi-directional many-to-one association to Programari
	@OneToMany(mappedBy="personalmedical")
	public List<Programari> getProgramaris() {
		return this.listaProgramari;
	}

	public void setProgramaris(List<Programari> programaris) {
		this.listaProgramari = programaris;
	}
	/**
	 * adaugam o noua programare
	 */

	public Programari addProgramari(Programari programari) {
		getProgramaris().add(programari);
		programari.setPersonalmedical(this);

		return programari;
	}

	/**
	 * stergem o programare
	 */
	public Programari removeProgramari(Programari programari) {
		getProgramaris().remove(programari);
		programari.setPersonalmedical(null);

		return programari;
	}

}