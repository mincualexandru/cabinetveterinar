package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the programari database table.
 * 
 */
@Entity
@NamedQuery(name="Programari.findAll", query="SELECT p FROM Programari p")
public class Programari implements Serializable {
	private static final long serialVersionUID = 1L;
	private int idprogramari;
	private Date data;
	private String diagnostic;
	private int ora;
	private Animal animal;
	private Personalmedical personalmedical;

	public Programari() {
	}


	@Id
	public int getIdprogramari() {
		return this.idprogramari;
	}

	public void setIdprogramari(int idprogramari) {
		this.idprogramari = idprogramari;
	}


	@Temporal(TemporalType.DATE)
	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}


	public String getDiagnostic() {
		return this.diagnostic;
	}

	public void setDiagnostic(String diagnostic) {
		this.diagnostic = diagnostic;
	}


	public int getOra() {
		return this.ora;
	}

	public void setOra(int ora) {
		this.ora = ora;
	}


	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="idAnimal")
	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}


	//bi-directional many-to-one association to Personalmedical
	@ManyToOne
	@JoinColumn(name="idPersonalMedical")
	public Personalmedical getPersonalmedical() {
		return this.personalmedical;
	}

	public void setPersonalmedical(Personalmedical personalmedical) {
		this.personalmedical = personalmedical;
	}

}