package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-01-14T23:00:44.563+0200")
@StaticMetamodel(Personalmedical.class)
public class Personalmedical_ {
	public static volatile SingularAttribute<Personalmedical, Integer> idPersonalMedical;
	public static volatile SingularAttribute<Personalmedical, String> function;
	public static volatile SingularAttribute<Personalmedical, String> name;
	public static volatile SingularAttribute<Personalmedical, Integer> salary;
	public static volatile ListAttribute<Personalmedical, Programari> programaris;
}
