package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@NamedQuery(name="Animal.findAll", query="SELECT a FROM Animal a")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;
	private int idAnimal;
	private String age;
	private String breed;
	private byte[] image;
	private String name;
	private List<Programari> listaProgramari;

	@Override
	public String toString() {
		return name ;
	}


	public Animal() {
	}


	@Id
	public int getIdAnimal() {
		return this.idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}


	public String getAge() {
		return this.age;
	}

	public void setAge(String age) {
		this.age = age;
	}


	public String getBreed() {
		return this.breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}


	public byte[] getImage() {
		return this.image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	//bi-directional many-to-one association to Programari
	@OneToMany(mappedBy="animal")
	public List<Programari> getProgramaris() {
		return this.listaProgramari;
	}

	public void setProgramaris(List<Programari> programaris) {
		this.listaProgramari = programaris;
	}

	public Programari addProgramari(Programari programari) {
		getProgramaris().add(programari);
		programari.setAnimal(this);

		return programari;
	}

	public Programari removeProgramari(Programari programari) {
		getProgramaris().remove(programari);
		programari.setAnimal(null);

		return programari;
	}

}