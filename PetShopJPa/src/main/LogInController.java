package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LogInController implements Initializable {

	@FXML private TextField utilizator;
	@FXML private PasswordField parola;
	@FXML private Button butonLogare;
	@FXML private Button butonInchidereFereastra;
	
	private Socket socket;
    private BufferedReader rezultat;
    private PrintWriter rezultatDate;
 
    private static final String loginAccepted = "Access granted";
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		try {
            socket = new Socket("localhost", 200);
            rezultat = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            rezultatDate =  new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            System.out.println("Conectarea la server a esuat !");
        }
		
			butonLogare.setOnMouseClicked(event -> {
		
				if (parola.getText() != null && utilizator.getText() != null) {
					
					String username = utilizator.getText();
					String pass = parola.getText();
					
					rezultatDate.println(username);
					rezultatDate.println(pass);
					
					try {
						if(rezultat.readLine().equals(loginAccepted)) {
							Scene currentScene=butonLogare.getScene();
							currentScene.getWindow().hide();
							
							FXMLLoader fxmlLoader = new FXMLLoader();
							fxmlLoader.setLocation(getClass().getResource("PetShopView.fxml"));
							Scene scene = new Scene(fxmlLoader.load());
							Stage stage = new Stage();
							stage.setTitle("Cabinet Veterinar");
							stage.setScene(scene);
							stage.show();
						}
						else {
							Scene currentScene=butonLogare.getScene();
							currentScene.getWindow().hide();
							
							FXMLLoader fxmlLoader = new FXMLLoader();
							fxmlLoader.setLocation(getClass().getResource("ErrorView.fxml"));
							Scene scene = new Scene(fxmlLoader.load());
							Stage stage = new Stage();
							stage.setTitle("Accesul nu este permis");
							stage.setScene(scene);
							stage.show();
						}
					    
					    } catch (IOException e) {
					    	System.out.println("Ceva nu a mers bine !");
					    }
				}
			});
			
		butonInchidereFereastra.setOnAction(event -> Platform.exit());
	}
}