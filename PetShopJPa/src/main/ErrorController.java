package main;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class ErrorController implements Initializable {
	
	@FXML private Button incercare;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		incercare.setOnMouseClicked(event -> {
			try {
				Scene currentScene=incercare.getScene();
				currentScene.getWindow().hide();
				
				FXMLLoader fxmlLoader = new FXMLLoader();
				fxmlLoader.setLocation(getClass().getResource("LogInView.fxml"));
				Scene scene = new Scene(fxmlLoader.load());
				Stage stage = new Stage();
				stage.setTitle("Cabinet Veterinar");
				stage.setScene(scene);
				stage.show();
		    
		    } catch (IOException e) {
		    	System.out.println("Ceva nu a mers bine !");
		    }
		});
	}
}