package main;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.Collection;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.Programari;
import util.AnimalUtil;
import util.DataBaseUtil;
import util.ProgramareUtil;

public class MainController implements Initializable {

	private ProgramareUtil dbProgramare = new ProgramareUtil();
	private AnimalUtil dbAnimal = new AnimalUtil();
	@FXML
	private TextField specie;

	@FXML
	private TextField varsta;

	@FXML
	private ImageView imagine;

	@FXML
	private ListView<Programari> programari = new ListView<>();

	@FXML
	private ListView<String> istoric = new ListView<>();

	@FXML
	private TextField nume;

	@FXML
	void inchidereFereastra(ActionEvent event) {

		Platform.exit();
	}

	private ObservableList<Programari> listaProgramari = FXCollections.observableArrayList();
	private ObservableList<Programari> listaDiagnostic = FXCollections.observableArrayList();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		listaProgramari.addAll(dbProgramare.afisareProgramariSortateData());
		//lista.addAll(dbProgramare.afisareProgramari());
		programari.setItems(listaProgramari);
		programari.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue)-> {
			specie.setText(newValue.getAnimal().getBreed());
			nume.setText(newValue.getAnimal().getName());
			varsta.setText(newValue.getAnimal().getAge());
			imagine.setImage(dbAnimal.afisareImagini(newValue));
			listaDiagnostic.clear();
			listaDiagnostic.addAll(dbProgramare.afisareDiagnostic());
			Collection<String> detalii = listaDiagnostic.stream().map(p -> MainController.toString(p)).collect(Collectors.toList());
			istoric.setItems(FXCollections.observableArrayList(detalii));
		});

		programari.setCellFactory(list -> new SpatiuProgramari());
	}

	public static String toString(Programari p) {
		return "Animalul " + p.getAnimal() + " care este sub supravegherea doctorului " + p.getPersonalmedical()
				+ " are diagnosticul " + p.getDiagnostic();
	}

	static class SpatiuProgramari extends ListCell<Programari> {
		@Override
		protected void updateItem(Programari programare, boolean booleanN) {
			super.updateItem(programare, booleanN);
			if (programare != null)
				setText("Programarea " + programare.getIdprogramari() + " in data de: " + programare.getData());
		}
	}
}
