package util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Personalmedical;
import model.Programari;

class ProgramareUtilTest {


	@Test
	void test1() {
		Programari programare = new Programari();
		programare.setOra(20);
		assertEquals(20, programare.getOra());
		
	}
	@Test
	void test2() {
		Programari programare = new Programari();
		programare.setDiagnostic("Picior rupt");;
		assertEquals("Picior rupt", programare.getDiagnostic());
		
	}
	@Test
	void test3() {
		Programari programare = new Programari();
		assertNotNull(programare);
	}

}
