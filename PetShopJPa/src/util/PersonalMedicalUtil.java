package util;

import java.util.List;

import model.Personalmedical;

public class PersonalMedicalUtil extends DataBaseUtil {

	/**
	 * Functie pentru inserarea unui personal medical in bd
	 * 
	 * @param pMedical personal medical care va fi inserat in bd
	 */
	public void createPersonalMedical(Personalmedical pMedical) {

		entityManager.getTransaction().begin();
		entityManager.persist(pMedical);
		entityManager.getTransaction().commit();

	}

	/**
	 * Functie pentru citirea unui personal medical
	 * 
	 * @param id Id-ul personalului medical care va fi returnat
	 * @return
	 */
	public Personalmedical readPersonalMedical(int id) {
		return entityManager.find(Personalmedical.class, id);
	}

	/**
	 * Functie pentru upgradarea unui personal medical
	 * 
	 * @param pMedical Personalul medical care exista in bd si care va fi upgradat
	 * @param name     Numele personalului medical care exista in bd si care va fi
	 *                 upgradat
	 * @param id       Id-ul personalului medical care exista in bd si care va fi
	 *                 upgradat
	 */
	public void updatePersonalMedicalName(Personalmedical pMedical, String name, int id, int salary, String function) {
		entityManager.getTransaction().begin();
		pMedical.setName(name);
		pMedical.setIdPersonalMedical(id);
		pMedical.setFunction(function);
		pMedical.setSalary(salary);
		entityManager.getTransaction().commit();
	}

	/**
	 * Functie pentru stergerea unui personal medical
	 * 
	 * @param pMedical Personalul medical care exista in bd si care va fi sters
	 */
	public void deletePersonalMedical(Personalmedical pMedical) {
		entityManager.getTransaction().begin();
		entityManager.remove(pMedical);
		entityManager.getTransaction().commit();
	}
	/**
	 * Functie pentru afisarea personalului medical 
	 */
	public void afisareaPersonaluluiMedical() {
		@SuppressWarnings("unchecked")
		List<Personalmedical> rezultatSql = entityManager
				.createNativeQuery("Select * from petshop.Personalmedical", Personalmedical.class).getResultList();
		for (Personalmedical personalMedical : rezultatSql) {
			System.out.println("Personalul medical :" + personalMedical.getName() + "are id-ul: "
					+ personalMedical.getIdPersonalMedical());
		}
	}
}
