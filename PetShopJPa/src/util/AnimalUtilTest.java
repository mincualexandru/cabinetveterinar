package util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Animal;

class AnimalUtilTest {

	@Test
	void test1() {
		Animal animal1 = new Animal();
		animal1.setName("Billy");
		assertEquals("Billy", animal1.getName());
		
	}
	@Test
	void test2() {
		Animal a=new Animal();
		assertNotNull(a);
	}
	@Test
	void test3() {
		Animal animal1 = new Animal();
		animal1.setBreed("pekinez");
		assertEquals("pekinez", animal1.getBreed());
		
	}
	@Test
	void test4() {
		Animal animal1 = new Animal();
		animal1.setAge("5");
		assertEquals("5", animal1.getAge());
		
	}

}
