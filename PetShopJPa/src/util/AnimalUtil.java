package util;

import java.io.ByteArrayInputStream;
import java.util.List;

import javafx.scene.image.Image;
import model.Animal;
import model.Programari;

public class AnimalUtil extends DataBaseUtil {

	/**
	 * Functie pentru inserarea unui animal in bd
	 * 
	 * @param animal Animal care va fi inserat in bd
	 */
	public void createAnimal(Animal animal) {

		entityManager.getTransaction().begin();
		entityManager.persist(animal);
		entityManager.getTransaction().commit();

	}

	/**
	 * Functie pentru citirea unui animal
	 * 
	 * @param id Id-ul animalului care va fi returnat
	 * @return
	 */
	public Animal readAnimal(int id) {
		return entityManager.find(Animal.class, id);
	}

	/**
	 * Functie pentru upgradrea unui animal
	 * 
	 * @param animal Animalul care exista in bd si care va fi upgradat
	 * @param name   Numele care exista in bd si care va fi upgradat
	 */
	public void updateAnimalName(Animal animal, String name, String age, String breed) {
		entityManager.getTransaction().begin();
		animal.setName(name);
		animal.setAge(age);
		animal.setBreed(breed);
		entityManager.getTransaction().commit();
	}

	/**
	 * Functie care sterge un animal din bd Animalul care exista in bd si care va fi
	 * sters
	 * 
	 * @param animal
	 */
	public void deleteAnimal(Animal animal) {
		entityManager.getTransaction().begin();
		entityManager.remove(animal);
		entityManager.getTransaction().commit();
	}
	/**
	 * Functie care returneaza numele animalului dintr-o programare
	 * @param programarea
	 */

	public String afisareNume(Programari prog) {
		return prog.getAnimal().getName();
	}
	/**
	 * Functie care returneaza specia animalului dintr-o programare
	 * @param programarea
	 */
	public String afisareaSpeciilor(Programari prog) {
		return prog.getAnimal().getBreed();
	}
	/**
	 * Functie care returneaza varsta animalului dintr-o programare
	 * @param programarea
	 */
	public String afisareaVarstelor(Programari prog) {
		return prog.getAnimal().getAge();
	}
	/**
	 * Functie care afiseaza toate animalele din baza de date
	 */
	public void afisareaAnimalelorDinBD() {
		@SuppressWarnings("unchecked")
		List<Animal> rezultatSql = entityManager.createNativeQuery("Select * from petshop.Animal", Animal.class)
				.getResultList();
		for (Animal animal : rezultatSql) {
			System.out.println("Animalul :" + animal.getName() + " are id-ul: " + animal.getIdAnimal());
		}
	}
	/**
	 * Functie care returneaza imaginea unui animal din baza de date
	 * @param programarea
	 */
	public Image afisareImagini(Programari programare) {
		Image imagine = null;
		@SuppressWarnings("unchecked")
		List<Animal> rezultatSql = entityManager.createNativeQuery(
				"Select * from PetShop.Animal, PetShop.Programari where PetShop.Animal.idAnimal = PetShop.Programari.idAnimal",
				Animal.class).getResultList();
		for (Animal animal : rezultatSql) {
			if (programare.getAnimal().getIdAnimal() == animal.getIdAnimal())
				imagine = new Image(new ByteArrayInputStream(animal.getImage()));
		}
		return imagine;
	}
}
