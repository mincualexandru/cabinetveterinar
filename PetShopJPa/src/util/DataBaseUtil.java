package util;

import java.io.ByteArrayInputStream;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import javafx.scene.image.Image;
import model.Animal;
import model.Personalmedical;
import model.Programari;

public class DataBaseUtil {

	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;

	public static void setUp() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("PetShopJPa");
		entityManager = entityManagerFactory.createEntityManager();

	}

	public static void closeEntityManager() {
		entityManager.close();
	}

	public static void startTransaction() {
		entityManager.getTransaction().begin();
	}

	public static void commitTransaction() {
		entityManager.getTransaction().commit();
	}
}
