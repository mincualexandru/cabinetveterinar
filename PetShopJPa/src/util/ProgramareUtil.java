package util;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import model.Animal;
import model.Personalmedical;
import model.Programari;

public class ProgramareUtil extends DataBaseUtil {

	/**
	 * Functie pentru inserarea unei programari in bd
	 * 
	 * @param pr programare care va fi inserata in bd
	 */
	public void createProgramari(Programari programare) {

		entityManager.getTransaction().begin();
		entityManager.persist(programare);
		entityManager.getTransaction().commit();

	}

	/**
	 * Functie pentru citirea unei programari
	 * 
	 * @param id Id-ul programarii care va fi returnata
	 * @return
	 */
	public Programari readProgramari(int id) {
		return entityManager.find(Programari.class, id);
	}

	/**
	 * Functie pentru upgradarea unei programari
	 * 
	 * @param programare        Programarea care exista in bd si care va fi
	 *                          upgradata
	 * @param data              Data existenta in bd si care va fi upgradata
	 * @param idAnimal          Id-ul animalului din bd care va fi upgradat
	 * @param idPersonalMedical Id-ul personalului medical din bd care va fi
	 *                          upgradat
	 */
	public void updateProgramari(Programari programare, Animal animal, Personalmedical personalMedical, int ora) {
		entityManager.getTransaction().begin();
		programare.setOra(ora);
		programare.setAnimal(animal);
		programare.setPersonalmedical(personalMedical);
		entityManager.getTransaction().commit();
	}

	/**
	 * Functie pentru stergerea unei programari
	 * 
	 * @param programare Programarea care exista in bd si care va fi stearsa...
	 */
	public void deleteProgramari(Programari programare) {
		entityManager.getTransaction().begin();
		entityManager.remove(programare);
		entityManager.getTransaction().commit();
	}
	/**
	 * Functie pentru afisarea tuturor programarilor din baza de date
	 */
	public void afisareProgramariBD() {
		@SuppressWarnings("unchecked")
		List<Programari> rezultatSql = entityManager.createNativeQuery("Select * from petshop.Programari", Programari.class)
				.getResultList();
		for (Programari programari : rezultatSql) {
			System.out.println("Programarea are id-ul: " + programari.getIdprogramari() + "in data de: "
					+ programari.getData() + " pentru animalul " + programari.getAnimal());
		}
	}
	/**
	 * Functie pentru afisarea tuturor programarilor
	 */
	public List<Programari> afisareProgramari() {
		return entityManager.createNativeQuery("Select * from PetShop.Programari", Programari.class).getResultList();
	}
	/**
	 * Functie pentru afisarea diagnosticului unui animal
	 */
	public List<Programari> afisareDiagnostic() {
		return entityManager.createNativeQuery(
				"Select * from PetShop.Animal, PetShop.Programari where PetShop.Animal.idAnimal = PetShop.Programari.idAnimal",
				Programari.class).getResultList();
	}
	/**
	 * Functie pentru afisarea programarilor in functie de data
	 */
	public List<Programari> afisareProgramariSortateData(){
		List<Programari> prog=afisareProgramari();
		Collections.sort(prog, (prog1, prog2) ->
				prog1.getData().compareTo(prog2.getData()));
		return prog;
	}

}
