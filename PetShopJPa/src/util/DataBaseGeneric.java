package util;

public class DataBaseGeneric<T extends DataBaseUtil> {
	/**
	 * cream o instanta 
	 */
	
	private static DataBaseGeneric<DataBaseUtil> instanta;
	
	/**
	 * initializam instanta
	 */
	
	static {
		try {
			instanta=new DataBaseGeneric<>();
		} catch (Exception e) {
			System.out.println("Ups! Se pare ca este o problema");
		}
	}
	/**
	 * returnam instanta
	 */
	public static DataBaseGeneric<DataBaseUtil> getInstance(){
		return instanta;
	}

	public DataBaseGeneric() throws Exception {

		DataBaseUtil.setUp();
		DataBaseUtil.startTransaction();
		DataBaseUtil.commitTransaction();

	}
	/**
	 * adaugam in baza de date
	 */
	public void addToDataBase(DataBaseGeneric<T> db) {
		DataBaseUtil.entityManager.persist(db);
	}

	public void closeEntity() {
		DataBaseUtil.closeEntityManager();
	}

}
