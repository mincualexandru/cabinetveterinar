package util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Animal;
import model.Personalmedical;

class PersonalMedicalUtilTest {

	@Test
	void test1() {
		Personalmedical pMedical = new Personalmedical();
		pMedical.setName("Alex");
		assertEquals("Alex", pMedical.getName());
		
	}
	@Test
	void test2() {
		Personalmedical pMedical = new Personalmedical();
		assertNotNull(pMedical);
	}
	@Test
	void test3() {
		Personalmedical pMedical = new Personalmedical();
		pMedical.setSalary(2000);
		assertEquals(2000, pMedical.getSalary());
		
	}
}
